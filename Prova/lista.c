#include "lista.h"

Tabela *tabela = NULL;
int posicao = 0, inst_cont = 0, label_cont = 0;
Instrucao *instrucoes;

Lista *inicializar_lista(char *id){
    Lista *novo = (Lista*)malloc(sizeof(Lista));
    novo->prox = NULL;
    strcpy(novo->id, id);
    
    return novo;
}

Lista *inserir_lista(char *id, Lista *l){
    Lista *novo = (Lista*)malloc(sizeof(Lista));
    Lista *aux = l;
    
    novo->prox = NULL;
    strcpy(novo->id, id);
    
    while(aux->prox!=NULL){
        aux = aux->prox;
    }
    
    aux->prox = novo;
    
    return l;
}

void atualizar_tabela(_tipo tipo, Lista *l){
    Lista *l_aux = l;
    while(l_aux != NULL){
        Tabela *novo = (Tabela*)malloc(sizeof(Tabela));
	
        strcpy(novo->id, l_aux->id);
        novo->pos = posicao;
        novo->tipo = tipo;
        novo->prox = NULL;

        if (tabela == NULL) {
            tabela = novo;
        } else {
            Tabela *t_aux = tabela;
            //verifica se o id já existe
            while (t_aux->prox != NULL) {
                if (strcmp(t_aux->id, novo->id) == 0) {
                    printf("Erro: Variável já declarada ->  %s = %s\n", t_aux->id, novo->id);
                    //se o id já existe, aborta a execução
                    exit(1);
                }
                t_aux = t_aux->prox;
            }
            t_aux->prox = novo;
        }
        l_aux = l_aux->prox;
        posicao++;
    }
}

_tipo verificar_tipo(char *id){
	Tabela *aux = tabela;/*
	printf("\n%d",TIPO_INT);
	printf("\n%d",TIPO_STRING);
	printf("\n%d",UNDEF);*/
	while(aux!=NULL){
		//printf("- ID: %s \t tipo: %d \n", aux->id, aux->tipo);
		if(strcmp(aux->id, id) == 0){
			//printf("%d - %s\n",aux->tipo, aux->id);
			if(aux->tipo == TIPO_INT)
				return TIPO_INT;
			else
				return TIPO_STRING;
		}
		aux=aux->prox;
	}
	printf("Erro: Símbolo não encontrado!\n");
}

void imprimir(){
    Tabela *aux = tabela;
    
    printf("-----# Tabela de Símbolos #-----\n");
    while(aux!=NULL){
        printf("- ID: %s \t Posição: %d \t", aux->id, aux->pos);
        if(aux->tipo == TIPO_INT)
            printf("Tipo: int\n");
        else
            printf("Tipo: String\n");
        aux = aux->prox;
    }
}

int encontrar_posicao(char *id){
	Tabela *aux = tabela;
	
	while(strcmp(aux->id,id)!=0){
		aux = aux->prox;
		
		if(aux==NULL){
			printf("Erro: Símbolo não encontrado! %s",id);
			exit(0);
		}
	}
	return aux->pos;
}

void gerar(_inst instrucao, int p1, int p2, char p3[512]){
	if(instrucoes == NULL)
		instrucoes = (Instrucao*)malloc(sizeof(Instrucao));
	else
		instrucoes = (Instrucao*)realloc(instrucoes,sizeof(Instrucao)*(inst_cont + 1));
	
	instrucoes[inst_cont].label = -1;
	instrucoes[inst_cont].instrucao = instrucao;
	instrucoes[inst_cont].p1 = p1;
	instrucoes[inst_cont].p2 = p2;
	strcpy(instrucoes[inst_cont].p3, p3);
	
	inst_cont++;
}

void corrigir(ListaCmd *l, int label){
	ListaCmd *aux = l;
	//printf("l%d\n", label);
	while(aux->prox != NULL){
		instrucoes[aux->pos].label = label;
		aux = aux->prox;
	}
	//if(aux->prox == NULL)
		//printf("a %d\n",aux->pos);
	instrucoes[aux->pos].label = label;
}

void gerarGOTOWhile(_inst instrucao, int label){
	if(instrucoes == NULL)
		instrucoes = (Instrucao*)malloc(sizeof(Instrucao));
	else
		instrucoes = (Instrucao*)realloc(instrucoes,sizeof(Instrucao)*(inst_cont + 1));
	
	instrucoes[inst_cont].label = label;
	instrucoes[inst_cont].instrucao = instrucao;
	instrucoes[inst_cont].p1 = -1;
	instrucoes[inst_cont].p2 = -1;
	strcpy(instrucoes[inst_cont].p3, "");
	
	inst_cont++;
}

int novoLabel(){
	label_cont++;
	gerar(LABEL, label_cont, -1, "");
	return label_cont;
}

ListaCmd *criarLista(ListaCmd *l, int proxInst){
	ListaCmd *aux = l;
	ListaCmd *novo = (ListaCmd*)malloc(sizeof(ListaCmd));
	
	if(aux!=NULL){
		while(aux->prox!=NULL){
			aux = aux->prox;
		}
		novo->prox = NULL;
		novo->pos = proxInst;
		aux->prox = novo;
	} else {
		novo->prox = NULL;
		novo->pos = proxInst;
		l=novo;
	}
	return l;
}

ListaCmd *merge(ListaCmd *l1, ListaCmd *l2){
	ListaCmd *aux = l1;
	
	while(aux->prox != NULL)
		aux = aux->prox;
	
	aux->prox = l2;
	
	return l1;
}

void print_inst(){
	printf("\nInstruções\n------------------------------\n");

    int i;
    for (i=0; i<inst_cont; i++){
        Instrucao id = instrucoes[i];

        if(id.instrucao == IMUL){
            printf("%d -> imul\n", i);
        } else if(id.instrucao == IADD){
            printf("%d -> iadd\n", i);
        } else if(id.instrucao == ISUB){
            printf("%d -> isub\n", i);
        } else if(id.instrucao == IDIV){
            printf("%d -> idiv\n", i);
        } else if(id.instrucao == IREM){
            printf("%d -> irem\n", i);
        } else if(id.instrucao == ILOAD){
            printf("%d -> iload %d\n", i, id.p1);
        } else if(id.instrucao == BIPUSH){
            printf("%d -> bipush %d\n", i, id.p1);
        } else if(id.instrucao == ISTORE){
            printf("%d -> istore %d\n", i, id.p1);
        } else if(id.instrucao == ICONST){
        	printf("%d -> iconst_%d\n", i, id.p1);
        } else if(id.instrucao == LDC){
			if(id.p1 == -1)
				printf("%d -> ldc %s\n", i, id.p3);
			else
        		printf("%d -> ldc %d\n", i, id.p1);
    	} else if(id.instrucao == ALOAD){
			printf("%d -> aload %d\n", i, id.p1);
		} else if(id.instrucao == ASTORE){
			printf("%d -> astore %d\n", i, id.p1);
		} else if(id.instrucao == GET_PRINT){
			printf("%d -> getstatic java/lang/System/out Ljava/io/PrintStream;\n", i);
		} else if(id.instrucao == ASTORE){
			printf("%d -> astore %d\n", i, id.p1);
		} else if(id.instrucao == PRINT){
			if(instrucoes[i-1].instrucao == ILOAD){
				printf("%d -> invokevirtual java/io/PrintStream/print(I)V\n", i);
			} else if(instrucoes[i-1].instrucao == ALOAD){
				printf("%d -> invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V\n", i);
			}else{
				if(instrucoes[i-1].instrucao == IADD || instrucoes[i-1].instrucao == ISUB || instrucoes[i-1].instrucao == IMUL || 
				instrucoes[i-1].instrucao == IDIV || instrucoes[i-1].instrucao == ICONST || instrucoes[i-1].instrucao == BIPUSH){
					printf("%d -> invokevirtual java/io/PrintStream/print(I)V\n", i);
				} else {
					if(instrucoes[i-1].p1 == -1){
						printf("%d -> invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V\n", i);
					} else {
						printf("%d -> invokevirtual java/io/PrintStream/print(I)V\n", i);
					}
				}
			}
		} else if(id.instrucao == PRINT_LIT){
			printf("%d -> ldc %s\n", i, id.p3);
			printf("%d -> invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V\n", i);
		} else if(id.instrucao == IFCMPLT){
			printf("%d -> if_icmplt l%d\n", i, id.label);
		} else if(id.instrucao == IFCMPLE){
			printf("%d -> if_icmple l%d\n", i, id.label);
		} else if(id.instrucao == IFCMPGT){
			printf("%d -> if_icmpgt l%d\n", i, id.label);
		} else if(id.instrucao == IFCMPGE){
			printf("%d -> if_icmpge l%d\n", i, id.label);
		} else if(id.instrucao == IFCMPEQ){
			printf("%d -> if_icmpeq l%d\n", i, id.label);
		} else if(id.instrucao == IFCMPNE){
			printf("%d -> if_icmpne l%d\n", i, id.label);
		} else if(id.instrucao == GOTO){
			printf("%d -> goto l%d\n", i, id.label);
			/*label++;
			fprintf("l%d:\n", label);*/
		}
    }
}

void gerar_jasmin(){
	int i, label=0;
	FILE *file;
	
	file=fopen("saida.j","w");
	if(!file){
		printf("Erro: Abertura arquivo de saída!");
	} else {
		fprintf(file, ".class public entradaJasmin\n");
		fprintf(file, ".super java/lang/Object\n\n");
		fprintf(file, ".method public <init>()V\n");
		fprintf(file, "\taload_0\n\n");
		fprintf(file, "\tinvokenonvirtual java/lang/Object/<init>()V\n");
		fprintf(file, "\treturn\n");
		fprintf(file, ".end method\n\n");
        fprintf(file, ".method public static main([Ljava/lang/String;)V\n");
        fprintf(file, "\t.limit stack %d\n",posicao+1);
        fprintf(file, "\t.limit locals %d\n",posicao);
        for(i=0; i<inst_cont; i++){
        	if(instrucoes[i].instrucao == LABEL){
                label++;
                fprintf(file,"l%d:\n",instrucoes[i].p1);
            }
            if(instrucoes[i].instrucao == IMUL){
		        fprintf(file, "\timul\n");
		    } else if(instrucoes[i].instrucao == IADD){
		        fprintf(file, "\tiadd\n");
		    } else if(instrucoes[i].instrucao == ISUB){
		        fprintf(file, "\tisub\n");
		    } else if(instrucoes[i].instrucao == IDIV){
		        fprintf(file, "\tidiv\n");
		    } else if(instrucoes[i].instrucao == IREM){
		        fprintf(file, "\tirem\n");
		    } else if(instrucoes[i].instrucao == ILOAD){
				fprintf(file, "\tiload %d\n", instrucoes[i].p1);
		    } else if(instrucoes[i].instrucao == BIPUSH){
		        fprintf(file, "\tbipush %d\n", instrucoes[i].p1);
		    } else if(instrucoes[i].instrucao == ISTORE){
		        fprintf(file, "\tistore %d\n", instrucoes[i].p1);
		    } else if(instrucoes[i].instrucao == ICONST){
		    	fprintf(file, "\ticonst_%d\n", instrucoes[i].p1);
		    } else if(instrucoes[i].instrucao == LDC){
				if(instrucoes[i].p1 == -1)
					fprintf(file, "\tldc %s\n", instrucoes[i].p3);
				else
		    		fprintf(file, "\tldc %d\n", instrucoes[i].p1);
			} else if(instrucoes[i].instrucao == ALOAD){
				fprintf(file, "\taload %d\n", instrucoes[i].p1);
			} else if(instrucoes[i].instrucao == GET_PRINT){
				fprintf(file, "\tgetstatic java/lang/System/out Ljava/io/PrintStream;\n");
			} else if(instrucoes[i].instrucao == ASTORE){
				fprintf(file, "\tastore %d\n", instrucoes[i].p1);
			} else if(instrucoes[i].instrucao == PRINT){
				if(instrucoes[i-1].instrucao == ILOAD){
					fprintf(file, "\tinvokevirtual java/io/PrintStream/println(I)V\n");
				} else if(instrucoes[i-1].instrucao == ALOAD){
					fprintf(file, "\tinvokevirtual java/io/PrintStream/print(Ljava/lang/String;)V\n");
				}else{
					if(instrucoes[i-1].instrucao == IADD || instrucoes[i-1].instrucao == ISUB || instrucoes[i-1].instrucao == IMUL || 
					instrucoes[i-1].instrucao == IDIV || instrucoes[i-1].instrucao == ICONST || instrucoes[i-1].instrucao == BIPUSH){
						fprintf(file, "\tinvokevirtual java/io/PrintStream/println(I)V\n");
					} else {
						if(instrucoes[i-1].p1 == -1){
							fprintf(file, "\tinvokevirtual java/io/PrintStream/print(Ljava/lang/String;)V\n");
						} else {
							fprintf(file, "\tinvokevirtual java/io/PrintStream/println(I)V\n");
						}
					}
				}
			} else if(instrucoes[i].instrucao == PRINT_LIT){
				fprintf(file, "\tldc %s\n", instrucoes[i].p3);
				fprintf(file, "\tinvokevirtual java/io/PrintStream/print(Ljava/lang/String;)V\n");
			} else if(instrucoes[i].instrucao == IFCMPLT){
				fprintf(file, "\tif_icmplt l%d\n", instrucoes[i].label);
			} else if(instrucoes[i].instrucao == IFCMPLE){
				fprintf(file, "\tif_icmple l%d\n", instrucoes[i].label);
			} else if(instrucoes[i].instrucao == IFCMPGT){
				fprintf(file, "\tif_icmpgt l%d\n", instrucoes[i].label);
			} else if(instrucoes[i].instrucao == IFCMPGE){
				fprintf(file, "\tif_icmpge l%d\n", instrucoes[i].label);
			} else if(instrucoes[i].instrucao == IFCMPEQ){
				fprintf(file, "\tif_icmpeq l%d\n", instrucoes[i].label);
			} else if(instrucoes[i].instrucao == IFCMPNE){
				fprintf(file, "\tif_icmpne l%d\n", instrucoes[i].label);
			} else if(instrucoes[i].instrucao == GOTO){
				fprintf(file, "\tgoto l%d\n", instrucoes[i].label);
			} else if(instrucoes[i].instrucao == GET_READ){
				//fprintf(file,"\tnew java/util/Scanner\n");
				//fprintf(file,"\tdup\n");
				//fprintf(file,"\tgetstatic java/lang/System/in Ljava/io/InputStream;\n");
				//fprintf(file,"\tinvokespecial java/util/Scanner/<init>(Ljava/io/InputStream;)V\n");	
			} else if(instrucoes[i].instrucao == READ_INT){
				//fprintf(file, "\tinvokevirtual java/util/Scanner/nextLine()Ljava/lang/String;\n");
			} else if(instrucoes[i].instrucao == READ_STR){
				//fprintf(file, "\tinvokevirtual java/util/Scanner/nextInt()I\n");
			}
        }
        fprintf(file,"\treturn\n.end method\n");
	}
	fclose(file);
}
