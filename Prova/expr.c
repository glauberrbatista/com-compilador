#include <stdio.h>

extern FILE *yyin;

int main(int argc, char **argv)
{   
    if (argc != 2){
        printf("Parâmetros inválidos.\n Modo de utilização: %s <arquivo>\n", argv[0]);
        return 0;
    }
	printf("Analisando o arquivo: %s\n",argv[1]);
	yyin = fopen(argv[1], "rt");
    if (yyin == NULL){
		printf("Problema com arquivo\n");
		return 1;
	}

	yyparse();
	fclose(yyin);
	return 0;
}

