%{
#include <stdio.h>
#include <stdlib.h>
#include "lista.h"
#define YYSTYPE atributo
%}

%token TADD TSUB TMUL TDIV TEQUAL TVIR TPV TAPAR TFPAR TACHA TFCHA TVOID TINT TSTR TIF TELSE TWHILE TPRINT TREAD TRET TCMPEQUAL TMEQUAL TMAEQUAL TMENOR TMAIOR TDIFF TOR TAND TNOT TFIM TID TLITERAL TNUM 

%%
Linha				: Programa TFIM {imprimir();gerar_jasmin();exit(0);}
					; 

Programa			: ListaFuncoes BlocoPrincipal          
					| BlocoPrincipal 
					;

ListaFuncoes		: ListaFuncoes Funcao 
					| Funcao 
					;

Funcao				: TipoRetorno TID TAPAR DeclParametros TFPAR BlocoPrincipal 
					| TipoRetorno TID TAPAR TFPAR BlocoPrincipal
					;

TipoRetorno			: Tipo 
					| TVOID 
					;

DeclParametros		: DeclParametros TVIR Parametro 
					| Parametro 
					;

Parametro			: Tipo TID
					;

BlocoPrincipal		: TACHA Declaracoes ListaCmd TFCHA 
					| TACHA ListaCmd TFCHA
					;

Declaracoes			: Declaracoes Declaracao 
					| Declaracao
					;

Declaracao			: Tipo ListaId TPV {atualizar_tabela($1.tipo, $2.listaID); }
					;

Tipo				: TINT {$$.tipo = TIPO_INT;}
					| TSTR {$$.tipo = TIPO_STRING;}
					;

ListaId				: ListaId TVIR TID {$$.listaID = inserir_lista($3.id, $$.listaID);}
					| TID {$$.listaID = inicializar_lista($1.id);}
					;
										
Bloco 				: TACHA ListaCmd TFCHA
					;

ListaCmd			: ListaCmd Comando  
					| Comando
					; 

Comando				: CmdSe 
					| CmdEnquanto 
					| CmdAtrib 
					| CmdEscrita 
					| CmdLeitura 
					| ChamadaFuncao 
					| Retorno 
					;
										
Retorno 			: TRET ExpA TPV
					;

CmdSe				: TIF TAPAR ExpL TFPAR M Bloco {corrigir($3.listaV, $5.label);corrigir($3.listaF, novoLabel());}
					| TIF TAPAR ExpL TFPAR M Bloco N TELSE M Bloco {corrigir($3.listaV, $5.label);corrigir($3.listaF, $9.label);corrigir($7.listaV, novoLabel());}
					;
					
N					: {$$.listaV = criarLista(proxInst);gerar(-1, GOTO, -1, -1, "");} ;
										
CmdEnquanto			: TWHILE TAPAR ExpL TFPAR Bloco 
					;

CmdAtrib			: TID TEQUAL ExpA TPV {gerar(-1, ISTORE, encontrar_posicao($1.id), -1, "");}
					| TID TEQUAL TLITERAL TPV {gerar(-1, LDC, -1, -1, $3.str); gerar(-1, ASTORE, encontrar_posicao($1.id), -1, $3.str);}
					;
										
CmdEscrita 			: TPRINT TAPAR PR ExpA TFPAR TPV {gerar(-1, PRINT_INT, encontrar_posicao($1.id), -1, "");}
					| TPRINT TAPAR PR TLITERAL TFPAR TPV {gerar(-1, PRINT_LIT, encontrar_posicao($1.id), -1, $4.str);}
					;

PR					: {gerar(-1, PRINT, -1, -1, "");};
										
CmdLeitura 			: TREAD TAPAR TID TFPAR TPV
					;

ChamadaFuncao		: TID TAPAR ListaParametros TFPAR TPV
					| TID TAPAR TFPAR TPV
					;
										
ListaParametros		: ListaParametros TVIR ExpA 
					| ExpA 
					;

ExpL				: ExpL TOR M TermoL {corrigir($1.listaF, $3.label); $$.listaV = merge($1.listaV, $4.listaV); $$.listaF = $4.listaF;}
					| ExpL TAND M TermoL {corrigir($1.listaV, $3.label); $$.listaF = merge($1.listaF, $4.listaF); $$.listaV = $4.listaV;}
					| TermoL {$$.listaV = $1.listaV; $$.listaF = $1.listaF;}
					;
												
TermoL 				: FatorL
					| TNOT TermoL {$$.listaV = $2.listaF; $$.listaF = $2;listaV;}
					;
					
FatorL				: TAPAR ExpL TFPAR {$$.listaV = $2.listaV; $$.listaF = $2.listaF;}
					| ExpR
					;

M					: {$$.label = novoLabel();};

ExpR				: ExpA TMENOR ExpA {$$.listaV = criarLista(proxInst); $$.listaF = criarLista(proxInst + 1); gerar(-1, IFCMPLT, -1, -1, "");gerar(-1, GOTO, -1, -1, "");}
					| ExpA TMAIOR ExpA {$$.listaV = criarLista(proxInst); $$.listaF = criarLista(proxInst + 1); gerar(-1, IFCMPGT, -1, -1, "");gerar(-1, GOTO, -1, -1, "");}
					| ExpA TMEQUAL ExpA {$$.listaV = criarLista(proxInst); $$.listaF = criarLista(proxInst + 1); gerar(-1, IFCMPLE, -1, -1, "");gerar(-1, GOTO, -1, -1, "");}
					| ExpA TMAEQUAL ExpA {$$.listaV = criarLista(proxInst); $$.listaF = criarLista(proxInst + 1); gerar(-1, IFCMPGE, -1, -1, "");gerar(-1, GOTO, -1, -1, "");}
					| ExpA TCMPEQUAL ExpA {$$.listaV = criarLista(proxInst); $$.listaF = criarLista(proxInst + 1); gerar(-1, IFCMPEQ, -1, -1, "");gerar(-1, GOTO, -1, -1, "");}
					| ExpA TDIFF ExpA {$$.listaV = criarLista(proxInst); $$.listaF = criarLista(proxInst + 1); gerar(-1, IFCMPNE, -1, -1, "");gerar(-1, GOTO, -1, -1, "");}
					;

ExpA				: ExpA TADD TermoA {gerar(-1, IADD, -1, -1, "");}
					| ExpA TSUB TermoA {gerar(-1, ISUB, -1, -1, "");}
					| TermoA
					;

TermoA				: TermoA TMUL FatorA {gerar(-1, IMUL, -1, -1, "");}
					| TermoA TDIV FatorA {gerar(-1, IDIV, -1, -1, "");}
					| FatorA
					;
												
FatorA				: TNUM {if($1.constante > -128 && $1.constante < 127)
								if($1.constante >= 1 && $1.constante <=5)
									gerar(-1, ICONST, $1.constante, -1, "");
								else
									gerar(-1, BIPUSH, $1.constante, -1, "");
							else
								gerar(-1, LDC, $1.constante, -1, "");
							}
					| TAPAR ExpA TFPAR 
					| TID TAPAR ListaParametros TFPAR
					| TID TAPAR TFPAR 
					| TID {gerar(-1, ILOAD, encontrar_posicao($1.id), -1, "");}
					;


%%
#include "lex.yy.c"

int yyerror (char *str)
{
	extern char *yytext;
	printf("%s - antes %s\n", str, yytext);
	
}  		 

int yywrap()
{
	return 1;
}
