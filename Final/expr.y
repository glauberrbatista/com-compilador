%{
#include <stdio.h>
#include <stdlib.h>
#include "lista.h"
#define YYSTYPE atributo
extern int inst_cont;
%}

%token TADD TSUB TMUL TDIV TMOD TEQUAL TVIR TPV TAPAR TFPAR TACHA TFCHA TVOID TINT TSTR TIF TELSE TWHILE TPRINT TREAD TRET TCMPEQUAL TMEQUAL TMAEQUAL TMENOR TMAIOR TDIFF TOR TAND TNOT TFIM TID TLITERAL TNUM 

%%
Linha				: Programa TFIM {imprimir();gerar_jasmin();exit(0);}
					; 

Programa			: ListaFuncoes BlocoPrincipal          
					| BlocoPrincipal 
					;

ListaFuncoes		: ListaFuncoes Funcao 
					| Funcao 
					;

Funcao				: TipoRetorno TID TAPAR DeclParametros TFPAR BlocoPrincipal 
					| TipoRetorno TID TAPAR TFPAR BlocoPrincipal
					;

TipoRetorno			: Tipo 
					| TVOID 
					;

DeclParametros		: DeclParametros TVIR Parametro 
					| Parametro 
					;

Parametro			: Tipo TID
					;

BlocoPrincipal		: TACHA Declaracoes ListaCmd TFCHA 
					| TACHA ListaCmd TFCHA
					;

Declaracoes			: Declaracoes Declaracao 
					| Declaracao
					;

Declaracao			: Tipo ListaId TPV {atualizar_tabela($1.tipo, $2.listaID); }
					;

Tipo				: TINT {$$.tipo = TIPO_INT;}
					| TSTR {$$.tipo = TIPO_STRING;}
					;

ListaId				: ListaId TVIR TID {$$.listaID = inserir_lista($3.id, $$.listaID);}
					| TID {$$.listaID = inicializar_lista($1.id);}
					;
										
Bloco 				: TACHA ListaCmd TFCHA
					;

ListaCmd			: ListaCmd Comando  
					| Comando
					; 

Comando				: CmdSe 
					| CmdEnquanto 
					| CmdAtrib 
					| CmdEscrita 
					| CmdLeitura 
					| ChamadaFuncao 
					| Retorno 
					;
										
Retorno 			: TRET ExpA TPV
					;

CmdSe				: TIF TAPAR ExpL TFPAR M Bloco {corrigir($3.listaV, $5.label);corrigir($3.listaF, novoLabel());}
					| TIF TAPAR ExpL TFPAR M Bloco N TELSE M Bloco {corrigir($3.listaV, $5.label);corrigir($3.listaF, $9.label);corrigir($7.listaV, novoLabel());}
					;
					
N					: {$$.listaV = criarLista($$.listaV,inst_cont);gerar(GOTO, -1, -1, "");} ;
										
CmdEnquanto			: TWHILE M TAPAR ExpL TFPAR M Bloco {corrigir($4.listaV, $6.label);gerarGOTOWhile(GOTO, $2.label);corrigir($4.listaF, novoLabel());}
					;

CmdAtrib			: TID TEQUAL ExpA TPV {
						if(verificar_tipo($1.id) == TIPO_INT)
							gerar(ISTORE, encontrar_posicao($1.id), -1, "");
						else
							gerar(ASTORE, encontrar_posicao($1.id), -1, "");
						}
					| TID TEQUAL TLITERAL TPV {gerar(LDC, -1, -1, $3.str); gerar(ASTORE, encontrar_posicao($1.id), -1, $3.str);}
					;
										
CmdEscrita 			: TPRINT TAPAR PR ExpA TFPAR TPV {gerar(PRINT, encontrar_posicao($4.id), -1, "");}
					| TPRINT TAPAR PR TLITERAL TFPAR TPV {gerar(PRINT_LIT, -1, -1, $4.str);}
					;

PR					: {gerar(GET_PRINT, -1, -1, "");};
										
CmdLeitura 			: TREAD TAPAR TID TFPAR TPV {
						gerar(GET_READ, -1, -1, "");
						if(verificar_tipo($3.id) == TIPO_INT){
							gerar(READ_INT, -1, -1, "");
							gerar(ISTORE, encontrar_posicao($3.id), -1, "");
						} else {
							gerar(READ_STR, -1, -1, "");
							gerar(ASTORE, encontrar_posicao($3.id), -1, "");
						}
							
					}
					;

ChamadaFuncao		: TID TAPAR ListaParametros TFPAR TPV
					| TID TAPAR TFPAR TPV
					;
										
ListaParametros		: ListaParametros TVIR ExpA 
					| ExpA 
					;

ExpL				: ExpL TOR M TermoL {corrigir($1.listaF, $3.label); $$.listaV = merge($1.listaV, $4.listaV); $$.listaF = $4.listaF;}
					| ExpL TAND M TermoL {corrigir($1.listaV, $3.label); $$.listaF = merge($1.listaF, $4.listaF); $$.listaV = $4.listaV;}
					| TermoL {$$.listaV = $1.listaV; $$.listaF = $1.listaF;}
					;
												
TermoL 				: FatorL
					| TNOT TermoL {$$.listaV = $2.listaF; $$.listaF = $2.listaV;}
					;
					
FatorL				: TAPAR ExpL TFPAR {$$.listaV = $2.listaV; $$.listaF = $2.listaF;}
					| ExpR
					;

M					: {$$.label = novoLabel();};

ExpR				: ExpA TMENOR ExpA {$$.listaV = criarLista($$.listaV,inst_cont); $$.listaF = criarLista($$.listaF,inst_cont + 1); gerar(IFCMPLT, -1, -1, "");gerar(GOTO, -1, -1, "");}
					| ExpA TMAIOR ExpA {$$.listaV = criarLista($$.listaV,inst_cont); $$.listaF = criarLista($$.listaF,inst_cont + 1); gerar(IFCMPGT, -1, -1, "");gerar(GOTO, -1, -1, "");}
					| ExpA TMEQUAL ExpA {$$.listaV = criarLista($$.listaV,inst_cont); $$.listaF = criarLista($$.listaF,inst_cont + 1); gerar(IFCMPLE, -1, -1, "");gerar(GOTO, -1, -1, "");}
					| ExpA TMAEQUAL ExpA {$$.listaV = criarLista($$.listaV,inst_cont); $$.listaF = criarLista($$.listaF,inst_cont + 1); gerar(IFCMPGE, -1, -1, "");gerar(GOTO, -1, -1, "");}
					| ExpA TCMPEQUAL ExpA {$$.listaV = criarLista($$.listaV,inst_cont); $$.listaF = criarLista($$.listaF,inst_cont + 1); gerar(IFCMPEQ, -1, -1, "");gerar(GOTO, -1, -1, "");}
					| ExpA TDIFF ExpA {$$.listaV = criarLista($$.listaV,inst_cont); $$.listaF = criarLista($$.listaF,inst_cont + 1); gerar(IFCMPNE, -1, -1, "");gerar(GOTO, -1, -1, "");}
					;

ExpA				: ExpA TADD TermoA {gerar(IADD, -1, -1, "");}
					| ExpA TSUB TermoA {gerar(ISUB, -1, -1, "");}
					| TermoA
					;

TermoA				: TermoA TMUL FatorA {gerar(IMUL, -1, -1, "");}
					| TermoA TDIV FatorA {gerar(IDIV, -1, -1, "");}
					| TermoA TMOD FatorA {gerar(IREM, -1, -1, "");}
					| FatorA
					;
												
FatorA				: TSUB FatorA {$$.constante = -$2.constante;}
					| TID {
						if(verificar_tipo($1.id) == TIPO_INT)
							gerar(ILOAD, encontrar_posicao($1.id), -1, "");
						else
							gerar(ALOAD, encontrar_posicao($1.id), -1, "");
						}
					| TNUM {
						if($1.constante > -128 && $1.constante < 127)
							if($1.constante >= 1 && $1.constante <=5)
								gerar(ICONST, $1.constante, -1, "");
							else
								gerar(BIPUSH, $1.constante, -1, "");
						else
							gerar(LDC, $1.constante, -1, "");
						}
					| TAPAR ExpA TFPAR 
					| ChamadaFuncao
					;

%%
#include "lex.yy.c"

int yyerror (char *str)
{
	extern char *yytext;
	printf("%s - antes %s\n", str, yytext);
	
}  		 

int yywrap()
{
	return 1;
}
