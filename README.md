# Compilador #

Projeto da disciplina de compiladores da UDESC que gera bytecodes Java montados pelo Jasmin.

São aceitos somente dois tipos de dados `int` e `string`.

Os diretórios contém as versões das etapas do trabalho, até a implementação final.

No diretório *Prova* está implementado o operador ternário e o incremento `+=`.