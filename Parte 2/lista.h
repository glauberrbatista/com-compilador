#ifndef LISTA_H
#define	LISTA_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef enum {TIPO_INT, TIPO_STRING, UNDEF} _tipo;

typedef struct __lista {
    struct __lista *prox;
    char id[256];
} Lista;

typedef struct __tabela {
    struct __tabela *prox;
    char id[256];
    int pos;
    _tipo tipo;
} Tabela;

typedef struct {
    char id[256];
    int constante;
    _tipo tipo;
    Lista *listaID;
} atributo;

Lista *inicializar_lista(char *id);
Lista *inserir_lista(char *id, Lista *l);
void atualizar_tabela(_tipo tipo, Lista *l);
void imprimir();
#endif	/* LISTA_H */
