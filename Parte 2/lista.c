#include "lista.h"

Tabela *tabela = NULL;
int posicao = 0;

Lista *inicializar_lista(char *id){
    Lista *novo = (Lista*)malloc(sizeof(Lista));
    novo->prox = NULL;
    strcpy(novo->id, id);
    
    return novo;
}

Lista *inserir_lista(char *id, Lista *l){
    Lista *novo = (Lista*)malloc(sizeof(Lista));
    Lista *aux = l;
    
    novo->prox = NULL;
    strcpy(novo->id, id);
    
    while(aux->prox!=NULL){
        aux = aux->prox;
    }
    
    aux->prox = novo;
    
    return l;
}

void atualizar_tabela(_tipo tipo, Lista *l){
    Lista *l_aux = l;
    while(l_aux != NULL){
        Tabela *novo = (Tabela*)malloc(sizeof(Tabela));
	
        strcpy(novo->id, l_aux->id);
        novo->pos = posicao;
        novo->tipo = tipo;
        novo->prox = NULL;

        if (tabela == NULL) {
            tabela = novo;
        } else {
            Tabela *t_aux = tabela;
            //verifica se o id já existe
            while (t_aux->prox != NULL) {
                if (strcmp(t_aux->id, novo->id) == 0) {
                    printf("Erro: Variável já declarada ->  %s = %s\n", t_aux->id, novo->id);
                    //se o id já existe, aborta a execução
                    exit(1);
                }
                t_aux = t_aux->prox;
            }
            t_aux->prox = novo;
        }
        l_aux = l_aux->prox;
        posicao++;
    }
}

void imprimir(){
    Tabela *aux = tabela;
    
    printf("-----# Tabela de Símbolos #-----\n");
    while(aux!=NULL){
        printf("- ID: %s \t Posição: %d \t", aux->id, aux->pos);
        if(aux->tipo == TIPO_INT)
            printf("Tipo: int\n");
        else
            printf("Tipo: String\n");
        aux = aux->prox;
    }
}
