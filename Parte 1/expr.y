%{
#include <stdio.h>
#include <stdlib.h>
%}

%token TADD TSUB TMUL TDIV TEQUAL TVIR TPV TAPAR TFPAR TACHA TFCHA TVOID TINT TSTR TIF TELSE TWHILE TPRINT TREAD TRET TCMPEQUAL TMEQUAL TMAEQUAL TMENOR TMAIOR TDIFF TOR TAND TNOT TFIM TID TLITERAL TNUM 

%%
Linha				: Programa TFIM {exit(0);}
					; 

Programa			: ListaFuncoes BlocoPrincipal          
					| BlocoPrincipal 
					;

ListaFuncoes		: ListaFuncoes Funcao 
					| Funcao 
					;

Funcao				: TipoRetorno TID TAPAR DeclParametros TFPAR BlocoPrincipal 
					| TipoRetorno TID TAPAR TFPAR BlocoPrincipal
					;

TipoRetorno			: Tipo 
					| TVOID 
					;

DeclParametros		: DeclParametros TVIR Parametro 
					| Parametro 
					;

Parametro			: Tipo TID
					;

BlocoPrincipal		: TACHA Declaracoes ListaCmd TFCHA 
					| TACHA ListaCmd TFCHA
					;

Declaracoes			: Declaracoes Declaracao 
					| Declaracao
					;
										
Declaracao			: Tipo ListaId TPV
					;

Tipo				: TINT
					| TSTR
					;

ListaId				: ListaId TVIR TID
					| TID
					;
										
Bloco 				: TACHA ListaCmd TFCHA
					;

ListaCmd			: ListaCmd Comando  
					| Comando
					; 

Comando				: CmdSe 
					| CmdEnquanto 
					| CmdAtrib 
					| CmdEscrita 
					| CmdLeitura 
					| ChamadaFuncao 
					| Retorno 
					;
										
Retorno 			: TRET ExpA TPV
					;

CmdSe				: TIF TAPAR ExpL TFPAR Bloco 
					| TIF TAPAR ExpL TFPAR Bloco TELSE Bloco 
					;
										
CmdEnquanto			: TWHILE TAPAR ExpL TFPAR Bloco 
					;

CmdAtrib			: TID TEQUAL ExpA TPV 
					| TID TEQUAL TLITERAL TPV
					;
										
CmdEscrita 			: TPRINT TAPAR ExpA TFPAR TPV 
					| TPRINT TAPAR TLITERAL TFPAR TPV
					;
										
CmdLeitura 			: TREAD TAPAR TID TFPAR TPV
					;

ChamadaFuncao		: TID TAPAR ListaParametros TFPAR TPV
					| TID TAPAR TFPAR TPV
					;
										
ListaParametros		: ListaParametros TVIR ExpA 
					| ExpA 
					;

ExpL				: ExpL TOR TermoL
					| ExpL TAND TermoL
					| TermoL
					;
												
TermoL 				: FatorL
					| TNOT TermoL
					;
					
FatorL				: TAPAR ExpL TFPAR
					| ExpR
					;

ExpR				: ExpA TMENOR ExpA
					| ExpA TMAIOR ExpA
					| ExpA TMEQUAL ExpA
					| ExpA TMAEQUAL ExpA
					| ExpA TCMPEQUAL ExpA
					| ExpA TDIFF ExpA
					;

ExpA				: ExpA TADD TermoA
					| ExpA TSUB TermoA
					| TermoA
					;

TermoA				: TermoA TMUL FatorA
					| TermoA TDIV FatorA
					| FatorA
					;
												
FatorA				: TNUM
					| TAPAR ExpA TFPAR
					| TID TAPAR ListaParametros TFPAR
					| TID TAPAR TFPAR
					| TID
					;


%%
#include "lex.yy.c"

int yyerror (char *str)
{
	printf("%s - antes %s\n", str, yytext);
	
}  		 

int yywrap()
{
	return 1;
}
