delim	[ \t]
ws	    {delim}+
digito	[0-9]
num	    {digito}+
alpha   [a-zA-Z]
literal \".*\"
id      ({alpha}|\_)({alpha}|{num}|\_)*

%%
"void"		{return TVOID;}
"int"		{return TINT;}
"String"	{return TSTR;}
"return" 	{return	TRET;}
"if"		{return TIF;}
"else"		{return TELSE;}
"while"		{return TWHILE;}
"read"		{return	TREAD;}
"print"		{return	TPRINT;}

"<="		{return TMEQUAL;}
">="		{return TMAEQUAL;}
"<"			{return TMENOR;}
">"			{return TMAIOR;}
"=="		{return TCMPEQUAL;}
"!="		{return TDIFF;}

"||"		{return TOR;}
"&&"		{return TAND;}
"!"			{return TNOT;}

"+"			{return TADD;}
"-"			{return TSUB;}
"*"			{return TMUL;}
"/"			{return TDIV;}

"="			{return	TEQUAL;}

";"			{return TPV;}
"("			{return TAPAR;}
")"			{return TFPAR;}
"{"			{return TACHA;}
"}"			{return TFCHA;}

{id} 		{return TID;}
{literal} 	{return TLITERAL;}
"," 		{return TVIR;}
{num}		{return TNUM;}
<<EOF>> 	{return TFIM;}
{ws}		{}
"\n"		{}
