#ifndef LISTA_H
#define	LISTA_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_INST 2000
typedef enum {TIPO_INT, TIPO_STRING, UNDEF} _tipo;
typedef enum {IMUL, IADD, ISUB, IDIV, BIPUSH, ILOAD, ISTORE, ICONST, LDC, ALOAD, ASTORE, PRINT_INT, PRINT_STR, PRINT_LIT, PRINT} _inst;

typedef struct __lista {
    struct __lista *prox;
    char id[256];
} Lista;

typedef struct __tabela {
    struct __tabela *prox;
    char id[256];
    int pos;
    _tipo tipo;
} Tabela;

typedef struct {
    char id[256];
    int constante;
    _tipo tipo;
    Lista *listaID;
    char str[512];
} atributo;

typedef struct {
	int label;
	_inst instrucao;
	int p1;
	int p2;
	char p3[512];
} Instrucao;

Lista *inicializar_lista(char *id);
Lista *inserir_lista(char *id, Lista *l);
void atualizar_tabela(_tipo tipo, Lista *l);
int encontrar_posicao(char *id);
void gerar(int label, _inst inst, int p1, int p2, char p3[512]);
void imprimir();
void print_inst();
#endif	/* LISTA_H */
