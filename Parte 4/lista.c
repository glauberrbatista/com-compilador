#include "lista.h"

Tabela *tabela = NULL;
int posicao = 0, inst_cont = 0;
Instrucao *instrucoes;

Lista *inicializar_lista(char *id){
    Lista *novo = (Lista*)malloc(sizeof(Lista));
    novo->prox = NULL;
    strcpy(novo->id, id);
    
    return novo;
}

Lista *inserir_lista(char *id, Lista *l){
    Lista *novo = (Lista*)malloc(sizeof(Lista));
    Lista *aux = l;
    
    novo->prox = NULL;
    strcpy(novo->id, id);
    
    while(aux->prox!=NULL){
        aux = aux->prox;
    }
    
    aux->prox = novo;
    
    return l;
}

void atualizar_tabela(_tipo tipo, Lista *l){
    Lista *l_aux = l;
    while(l_aux != NULL){
        Tabela *novo = (Tabela*)malloc(sizeof(Tabela));
	
        strcpy(novo->id, l_aux->id);
        novo->pos = posicao;
        novo->tipo = tipo;
        novo->prox = NULL;

        if (tabela == NULL) {
            tabela = novo;
        } else {
            Tabela *t_aux = tabela;
            //verifica se o id já existe
            while (t_aux->prox != NULL) {
                if (strcmp(t_aux->id, novo->id) == 0) {
                    printf("Erro: Variável já declarada ->  %s = %s\n", t_aux->id, novo->id);
                    //se o id já existe, aborta a execução
                    exit(1);
                }
                t_aux = t_aux->prox;
            }
            t_aux->prox = novo;
        }
        l_aux = l_aux->prox;
        posicao++;
    }
}

void imprimir(){
    Tabela *aux = tabela;
    
    printf("-----# Tabela de Símbolos #-----\n");
    while(aux!=NULL){
        printf("- ID: %s \t Posição: %d \t", aux->id, aux->pos);
        if(aux->tipo == TIPO_INT)
            printf("Tipo: int\n");
        else
            printf("Tipo: String\n");
        aux = aux->prox;
    }
}

int encontrar_posicao(char *id){
	Tabela *aux = tabela;
	
	while(strcmp(aux->id,id)!=0){
		aux = aux->prox;
		
		if(aux==NULL){
			printf("Erro: Símbolo não encontrado! %s",id);
			exit(0);
		}
	}
	return aux->pos;
}

void gerar(int label, _inst instrucao, int p1, int p2, char p3[512]){
	if(instrucoes == NULL)
		instrucoes = (Instrucao*)malloc(sizeof(Instrucao));
	else
		instrucoes = (Instrucao*)realloc(instrucoes,sizeof(Instrucao)*(inst_cont + 1));
	
	instrucoes[inst_cont].label = label;
	instrucoes[inst_cont].instrucao = instrucao;
	instrucoes[inst_cont].p1 = p1;
	instrucoes[inst_cont].p2 = p2;
	strcpy(instrucoes[inst_cont].p3, p3);
	
	inst_cont++;
}

void print_inst(){
	printf("\nInstruções\n------------------------------\n");

    int i;
    for (i=0; i<inst_cont; i++){
        Instrucao id = instrucoes[i];

        if(id.instrucao == IMUL){
            printf("%d -> IMUL\n", i);
        } else if(id.instrucao == IADD){
            printf("%d -> IADD\n", i);
        } else if(id.instrucao == ISUB){
            printf("%d -> ISUB\n", i);
        } else if(id.instrucao == IDIV){
            printf("%d -> IDIV\n", i);
        } else if(id.instrucao == ILOAD){
            printf("%d -> ILOAD %d\n", i, id.p1);
        } else if(id.instrucao == BIPUSH){
            printf("%d -> BIPUSH %d\n", i, id.p1);
        } else if(id.instrucao == ISTORE){
            printf("%d -> ISTORE %d\n", i, id.p1);
        } else if(id.instrucao == ICONST){
        	printf("%d -> ICONST_%d\n", i, id.p1);
        } else if(id.instrucao == LDC){
			if(strcmp(id.p3, "") != 0)
				printf("%d -> LDC %s\n", i, id.p3);
			else
        		printf("%d -> LDC %d\n", i, id.p1);
    	} else if(id.instrucao == ALOAD){
			printf("%d -> aload %d\n", i, id.p1);
		} else if(id.instrucao == ASTORE){
			printf("%d -> astore %d\n", i, id.p1);
		} else if(id.instrucao == PRINT_INT){
			printf("%d -> getstatic java/lang/System/out Ljava/io/PrintStream;\n", i);
			printf("%d -> iload %d\n", i, id.p1);
			printf("%d -> invokevirtual java/io/PrintStream/println(I)V\n", i);
		} else if(id.instrucao == PRINT_STR){
			printf("%d -> getstatic java/lang/System/out Ljava/io/PrintStream;\n", i);
			printf("%d -> aload %d\n", i, id.p1);
			printf("%d -> invokevirtual invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V\n", i);
		} else if(id.instrucao == PRINT_LIT){
			printf("%d -> getstatic java/lang/System/out Ljava/io/PrintStream;\n", i);
			printf("%d -> ldc %s\n", i, id.p3);
			printf("%d -> invokevirtual invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V\n", i);
		}
    }
}

void gerar_jasmin(){
	int i, label;
	FILE *file;
	
	file=fopen("saida.j","w");
	if(!file){
		printf("Erro: Abertura arquivo de saída!");
	} else {
		fprintf(file, ".class public entradaJasmin\n");
		fprintf(file, ".super java/lang/Object\n\n");
		fprintf(file, ".method public <init>()V\n");
		fprintf(file, "\taload_0\n\n");
		fprintf(file, "\tinvokenonvirtual java/lang/Object/<init>()V\n");
		fprintf(file, "\treturn\n");
		fprintf(file, ".end method\n\n");
        fprintf(file, ".method public static main([Ljava/lang/String;)V\n");
        fprintf(file, "\t.limit stack 5\n");
        fprintf(file, "\t.limit locals %d\n",posicao);
        for(i=0; i<inst_cont; i++){
        	if(instrucoes[i].label!=-1){
                label++;
                fprintf(file," l%i\n",label);
            }
            if(instrucoes[i].instrucao == IMUL){
		        fprintf(file, "\timul\n");
		    } else if(instrucoes[i].instrucao == IADD){
		        fprintf(file, "\tiadd\n");
		    } else if(instrucoes[i].instrucao == ISUB){
		        fprintf(file, "\tisub\n");
		    } else if(instrucoes[i].instrucao == IDIV){
		        fprintf(file, "\tidiv\n");
		    } else if(instrucoes[i].instrucao == ILOAD){
				fprintf(file, "\tiload %d\n", instrucoes[i].p1);
		    } else if(instrucoes[i].instrucao == BIPUSH){
		        fprintf(file, "\tbipush %d\n", instrucoes[i].p1);
		    } else if(instrucoes[i].instrucao == ISTORE){
		        fprintf(file, "\tistore %d\n", instrucoes[i].p1);
		    } else if(instrucoes[i].instrucao == ICONST){
		    	fprintf(file, "\ticonst_%d\n", instrucoes[i].p1);
		    } else if(instrucoes[i].instrucao == LDC){
				if(strcmp(instrucoes[i].p3, "") != 0)
					fprintf(file, "\tldc %s\n", instrucoes[i].p3);
				else
		    		fprintf(file, "\tldc %d\n", instrucoes[i].p1);
			} else if(instrucoes[i].instrucao == ALOAD){
				fprintf(file, "\taload %d\n", instrucoes[i].p1);
			} else if(instrucoes[i].instrucao == PRINT){
				fprintf(file, "\tgetstatic java/lang/System/out Ljava/io/PrintStream;\n");
			} else if(instrucoes[i].instrucao == ASTORE){
				fprintf(file, "\tastore %d\n", instrucoes[i].p1);
			} else if(instrucoes[i].instrucao == PRINT_INT){
				fprintf(file, "\tinvokevirtual java/io/PrintStream/println(I)V\n");
			} else if(instrucoes[i].instrucao == PRINT_STR){
				fprintf(file, "\tinvokevirtual java/io/PrintStream/print(Ljava/lang/String;)V\n");
			} else if(instrucoes[i].instrucao == PRINT_LIT){
				fprintf(file, "\tldc %s\n", instrucoes[i].p3);
				fprintf(file, "\tinvokevirtual java/io/PrintStream/print(Ljava/lang/String;)V\n");
			}
        }
        fprintf(file,"\treturn\n.end method\n");
	}
	fclose(file);
}
