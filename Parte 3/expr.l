delim	[ \t\n]
ws	    {delim}+
digito	[0-9]
num	    [+-]?{digito}+
alpha   [a-zA-Z]
literal \".*\"
id      ({alpha}|\_)({alpha}|{num}|\_)*

%%
{ws}		{}
"void"		{return TVOID;}
"int"		{return TINT;}
"String"	{return TSTR;}
"return" 	{return	TRET;}
"if"		{return TIF;}
"else"		{return TELSE;}
"while"		{return TWHILE;}
"read"		{return	TREAD;}
"print"		{return	TPRINT;}

"<="		{return TMEQUAL;}
">="		{return TMAEQUAL;}
"<"			{return TMENOR;}
">"			{return TMAIOR;}
"=="		{return TCMPEQUAL;}
"!="		{return TDIFF;}

"||"		{return TOR;}
"&&"		{return TAND;}
"!"			{return TNOT;}

"+"			{return TADD;}
"-"			{return TSUB;}
"*"			{return TMUL;}
"/"			{return TDIV;}

"="			{return	TEQUAL;}
"," 		{return TVIR;}

";"			{return TPV;}
"("			{return TAPAR;}
")"			{return TFPAR;}
"{"			{return TACHA;}
"}"			{return TFCHA;}

<<EOF>> 	{return TFIM;}
{literal} 	{strcpy(yylval.str,yytext); return TLITERAL;}
{num}		{yylval.constante = atoi(yytext); yylval.tipo = TIPO_INT; return TNUM;}
.			{printf("Erro antes %s\n", yytext); exit(0);}
