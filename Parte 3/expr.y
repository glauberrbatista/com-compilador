%{
#include <stdio.h>
#include <stdlib.h>
#include "lista.h"
#define YYSTYPE atributo
%}

%token TADD TSUB TMUL TDIV TEQUAL TVIR TPV TAPAR TFPAR TACHA TFCHA TVOID TINT TSTR TIF TELSE TWHILE TPRINT TREAD TRET TCMPEQUAL TMEQUAL TMAEQUAL TMENOR TMAIOR TDIFF TOR TAND TNOT TFIM TID TLITERAL TNUM 

%%
Linha				: Programa TFIM {imprimir();print_inst();gerar_jasmin();exit(0);}
					; 

Programa			: ListaFuncoes BlocoPrincipal          
					| BlocoPrincipal 
					;

ListaFuncoes		: ListaFuncoes Funcao 
					| Funcao 
					;

Funcao				: TipoRetorno TID TAPAR DeclParametros TFPAR BlocoPrincipal 
					| TipoRetorno TID TAPAR TFPAR BlocoPrincipal
					;

TipoRetorno			: Tipo 
					| TVOID 
					;

DeclParametros		: DeclParametros TVIR Parametro 
					| Parametro 
					;

Parametro			: Tipo TID {$$.listaID = inserir_lista($2.id, $$.listaID);}
					;

BlocoPrincipal		: TACHA Declaracoes ListaCmd TFCHA 
					| TACHA ListaCmd TFCHA
					;

Declaracoes			: Declaracoes Declaracao 
					| Declaracao
					;

Declaracao			: Tipo ListaId TPV {atualizar_tabela($1.tipo, $2.listaID); }
					;

Tipo				: TINT {$$.tipo = TIPO_INT;}
					| TSTR {$$.tipo = TIPO_STRING;}
					;

ListaId				: ListaId TVIR TID {$$.listaID = inserir_lista($3.id, $$.listaID);}
					| TID {$$.listaID = inicializar_lista($1.id);}
					;
										
Bloco 				: TACHA ListaCmd TFCHA
					;

ListaCmd			: ListaCmd Comando  
					| Comando
					; 

Comando				: CmdSe 
					| CmdEnquanto 
					| CmdAtrib 
					| CmdEscrita 
					| CmdLeitura 
					| ChamadaFuncao 
					| Retorno 
					;
										
Retorno 			: TRET ExpA TPV {gerar(-1,ISTORE,encontrar_posicao($2.id),-1);}
					;

CmdSe				: TIF TAPAR ExpL TFPAR Bloco 
					| TIF TAPAR ExpL TFPAR Bloco TELSE Bloco 
					;
										
CmdEnquanto			: TWHILE TAPAR ExpL TFPAR Bloco 
					;

CmdAtrib			: TID TEQUAL ExpA TPV {gerar(-1, ISTORE, encontrar_posicao($1.id), -1);}
					| TID TEQUAL TLITERAL TPV 
					;
										
CmdEscrita 			: TPRINT TAPAR ExpA TFPAR TPV 
					| TPRINT TAPAR TLITERAL TFPAR TPV
					;
										
CmdLeitura 			: TREAD TAPAR TID TFPAR TPV
					;

ChamadaFuncao		: TID TAPAR ListaParametros TFPAR TPV
					| TID TAPAR TFPAR TPV
					;
										
ListaParametros		: ListaParametros TVIR ExpA 
					| ExpA 
					;

ExpL				: ExpL TOR TermoL
					| ExpL TAND TermoL
					| TermoL
					;
												
TermoL 				: FatorL
					| TNOT TermoL
					;
					
FatorL				: TAPAR ExpL TFPAR
					| ExpR
					;

ExpR				: ExpA TMENOR ExpA
					| ExpA TMAIOR ExpA
					| ExpA TMEQUAL ExpA
					| ExpA TMAEQUAL ExpA
					| ExpA TCMPEQUAL ExpA
					| ExpA TDIFF ExpA
					;

ExpA				: ExpA TADD TermoA {gerar(-1, IADD, -1, -1);}
					| ExpA TSUB TermoA {gerar(-1, ISUB, -1, -1);}
					| TermoA
					;

TermoA				: TermoA TMUL FatorA {gerar(-1, IMUL, -1, -1);}
					| TermoA TDIV FatorA {gerar(-1, IDIV, -1, -1);}
					| FatorA
					;
												
FatorA				: TNUM {if($1.constante > -128 && $1.constante < 127)
								if($1.constante >= 0 && $1.constante <=5)
									gerar(-1, ICONST, $1.constante, -1);
								else
									gerar(-1, BIPUSH, $1.constante, -1);
							else
								gerar(-1, LDC, $1.constante, -1);
							}
					| TAPAR ExpA TFPAR 
					| TID TAPAR ListaParametros TFPAR
					| TID TAPAR TFPAR 
					| TID {gerar(-1, ILOAD, encontrar_posicao($1.id), -1);}
					;


%%
#include "lex.yy.c"

int yyerror (char *str)
{
	extern char *yytext;
	printf("%s - antes %s\n", str, yytext);
	
}  		 

int yywrap()
{
	return 1;
}
